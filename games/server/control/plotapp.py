#! /usr/bin/env python
# -*- coding: UTF8 -*-
# Este arquivo é parte do programa SuperPython
# Copyright 2013-2018 Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
#
# SuperPython é um software livre; você pode redistribuí-lo e/ou
# modificá-lo dentro dos termos da Licença Pública Geral GNU como
# publicada pela Fundação do Software Livre (FSF); na versão 2 da
# Licença.
#
# Este programa é distribuído na esperança de que possa ser útil,
# mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
# a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
# Licença Pública Geral GNU para maiores detalhes.
#
# Você deve ter recebido uma cópia da Licença Pública Geral GNU
# junto com este programa, se não, veja em <http://www.gnu.org/licenses/>

"""Request handler for plot service.

.. moduleauthor:: Carlo Oliveira <carlo@nce.ufrj.br>

"""
from bottle import route, run, default_app, get, static_file, view, TEMPLATE_PATH
from plotter import plot
import os
project_server = os.path.dirname(os.path.abspath(__file__))
# make sure the default templates directory is known to Bottle
templates_dir = os.path.join(project_server, '../view')
js_dir = os.path.join(project_server, '../stlib')
img_dir = os.path.join(project_server, '../image')
print(templates_dir)

if templates_dir not in TEMPLATE_PATH:
    TEMPLATE_PATH.insert(0, templates_dir)


@route('/')
@view('plot')
def index():
    return {}


# Static Routes
@get("<filepath:re:.*\.py>")
def py(filepath):
    return static_file(filepath, root=templates_dir)


# Static Routes
@get("/stlib/<filepath:re:.*\.(js|css)>")
def ajs(filepath):
    return static_file(filepath, root=js_dir)


# Static Routes
@get("<filepath:re:.*\.(js|css)>")
def js(filepath):
    return static_file(filepath, root=js_dir)


# Static Routes
@get("/image/<filepath:re:.*\.(png|jpg|svg|gif|ico)>")
def img(filepath):
    return static_file(filepath, root=img_dir)

SVG="""<?xml version="1.0"?>
<svg height="600" width="600" xmlns="http://www.w3.org/2000/svg">
  <circle cx="50%" cy="50%" r="50%" fill="red" />
</svg>
"""
# Static Routes
@get("/plot/<filepath:re:.*\.(png|jpg|svg|gif|ico)>")
def plt(filepath):
    return plot()


if __name__ == "__main__":
    run(host="localhost", port=8080)
else:
    application = default_app()
