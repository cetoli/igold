from tkinter import *
from PIL import Image, ImageTk
import serial

DX = 250


class BotaoLed:
    ESCOLHIDOS = []

    def __init__(self, cor='Azul', fundo='blue', x=30, y=100):
        self.xy = dict(x=x, y=y)
        self.desligado = icone1 = ImageTk.PhotoImage(file='Botao{}.jpg'.format(cor))
        self.ligado = ImageTk.PhotoImage(file='Botao{}Lig.jpg'.format(cor))
        self.text = 'Ligar Led {}'.format(cor)
        self.fundo, self.cor = fundo, cor
        self.btn = b = Button(text=self.text, command=lambda: self.escolhe_botao(self))  # , fg='white', bg=fundo)
        self.btn.config(image=icone1, highlightthickness=0, bd=0)
        b.image = icone1
        b.pack()
        self.btn.place(x=x, y=y)
        # botao1.bind("<Button-1>", escolhe_botao)

    def escolhe_botao(self, _):
        BotaoLed.ESCOLHIDOS.append(self)
        self.btn.config(image=self.ligado, highlightthickness=0, bd=0)
        self.btn.image = self.ligado
        self.btn.pack()
        self.btn.place(**self.xy)
        print(self.fundo, len(BotaoLed.ESCOLHIDOS))

    def desliga(self):
        self.btn.config(image=self.desligado, highlightthickness=0, bd=0)
        self.btn.image = self.desligado
        self.btn.pack()
        self.btn.place(**self.xy)


class BotaoEscolhe:
    ESCOLHIDOS = []

    def __init__(self, fundo='white', x=30, y=400):
        self.xy = dict(x=x, y=y)
        self.desligado = icone1 = ImageTk.PhotoImage(file='CartaVazia.jpg')
        self.text = 'Clique Aqui Escolha'
        self.fundo = self.cor = fundo
        self.btn = b = Button(text=self.text, command=lambda: self.escolhe_botao(self), fg='black', bg=fundo)
        self.btn.config(image=icone1, highlightthickness=10, bd=0)
        b.image = icone1
        b.pack()
        self.btn.place(x=x, y=y)

    def escolhe_botao(self, _):
        if not BotaoLed.ESCOLHIDOS:
            return
        botao = BotaoLed.ESCOLHIDOS.pop(0)
        BotaoEscolhe.ESCOLHIDOS.append(botao)
        self.btn.config(fg='white', bg=botao.fundo)
        self.fundo, self.cor = botao.fundo, botao.cor
        self.btn.config(image=botao.ligado, highlightthickness=0, bd=0)
        self.btn.image = botao.ligado
        self.btn.pack()
        self.btn.place(**self.xy)
        botao.desliga()
        print(botao.fundo, len(BotaoEscolhe.ESCOLHIDOS))

    def desliga(self):
        self.btn.config(image=self.desligado, highlightthickness=0, bd=0)
        self.btn.image = self.desligado
        self.btn.pack()
        self.btn.place(**self.xy)


class Arduled:
    def __init__(self):
        self.portaUSB = self
        self.tela = tela = Tk()
        porta_arduino = Label(text='Informe a Porta do Arduíno:').place(x=240, y=20)
        self.temp = StringVar()

        botao_porta = Button(text='OK', command=self.criar_porta).place(x=600, y=20)
        porta = Entry(tela, textvariable=self.temp).place(x=430, y=20)

        self.cores = [cor.split(":") for cor in "Azul:blue Verde:green Vermelho:red Amarelo:yellow".split()]

        self.comandos = "Azul Verde Vermelho Amarelo".split()

        tela.title('Programação Infantil')

        tela.geometry('1000x1000')

        self.botoes = [BotaoLed(cor, fundo, 30 + x * DX) for x, (cor, fundo) in enumerate(self.cores)]
        self.escolhas = [BotaoEscolhe(x=30 + x * DX) for x, *_ in enumerate(self.cores)]

        self.envia = Button(text='vai', command=self.envia_programa).place(x=700, y=20)

    def write(self, param):
        print("NÃO enviou para arduino", param)

    def criar_porta(self):
        # global portaUSB
        aux = self.temp.get()
        self.portaUSB = serial.Serial(aux, 9600)
        # 9600 = velocidade normalmente utilizada no arduíno
        print("porta_arduino", aux)

    def envia_programa(self):
        for comando in self.escolhas:
            comando.desliga()
            if comando.cor in self.comandos:
                self.envia_comando("01{}".format(self.comandos.index(comando.cor)))
                # pass

    def main(self):

        self.tela.mainloop()

    def envia_comando(self, param):
        aux = str(param)
        self.portaUSB.write(aux.encode())
        print(param)
        # pass


'''Códigos estipulados para a comunicação com o Arduíno:

AzulON - 010
VerdeON - 011
VermelhoON - 012
AmareloON - 013
AzulOFF - 014
VerdeOFF - 015
VermelhoOFF - 016
AmareloOFF - 017

'''

if __name__ == '__main__':
    Arduled().main()
