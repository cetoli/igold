<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><!--

############################################################
Glowfish - A 3D game as variation of tic-tac-toe
############################################################

:Author: *Carlo E. T. Oliveira*
:Contact: carlo@nce.ufrj.br
:Date: 2014/05/07
:Status: This is a "work in progress"
:Home: `Labase <http://labase.selfip.org/>`__
:Copyright: 2014, `GPL <http://is.gd/3Udt>`__.

In GlowFish you combine the properties of blobs to win.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>GlowFish 3D Game</title>
        <meta http-equiv="content-type" content="application/xml;charset=utf-8" />
        <script type="text/javascript" src="stlib/brython.js"></script>
        <script type="text/javascript" src="stlib/brython_stlib.js"></script>
        <script type="text/python">
            from browser import ajax, html, document
            # from plot import main
            class Brython:
                ajax, html, document = ajax, html, document
            # main(Brython)
        </script>
    </head>
    <body onLoad="brython(1)">
        <div id="pydev" class="glowscript">
            <object data="/plot/svg.svg" type="image/svg+xml" width="900px" height="900px"></object>

        </div>
    </body>
</html>
